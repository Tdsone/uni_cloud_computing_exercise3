module.exports = function (options) {
    //Import the mock data json file
    const mockData = require('./MOCK_DATA.json');

    //Add the patterns and their corresponding functions
    this.add('role:product,cmd:getProductURL', productURL);
    this.add('role:product,cmd:getProductName', productName);


    //TODO: add the pattern functions and describe the logic inside the function
    const productURL = (msg, respond) => {
        for(var p of mockData){
            if(msg.productId == p.product_id){
                respond(null, {result: p.product_url})
                return;
            }
        }
        respond("Item not found", null)
    }

    const productName = (msg, respond) => {
        
    }
}