module.exports = function (options) {
    //Import the mock data json file
    const mockData = require('./MOCK_DATA.json');

    //To DO: Add the patterns and their corresponding functions
    this.add('role:product,cmd:getProductPrice', productPrice);

    //To DO: add the pattern functions and describe the logic inside the function
    const productPrice = (msg, respond) => {
        if(msg.productId){
            for(var p of mockData){
                if(msg.productId == p.product_price){
                    respond(null, {result: p.product_price})
                    return
                }
            }
            respond(null, {result: ""})
        } else {
            respond(null, {result: ''})
        }
    }
}